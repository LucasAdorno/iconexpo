$(function()
{
    $("#contact_form").submit(function(event)
    {
        // event.preventDefault();
        // grecaptcha.reset();
        grecaptcha.ready(function() {
            grecaptcha.execute('6LfVNcEUAAAAAFT89GVvYmmFd0zkJ-LoGM5o9K8U', {action: 'homepage'}).then(function(token) {
                
                $('#token').val(token);

                var form = $("#contact_form");
                var str = form.serialize();

                $.ajax(
                {
                    type: "POST",
                    url: "contact.php",
                    data: str,
                    success: function(msg)
                    {
                        $("#feedback .result .alert").remove();
                        msg = JSON.parse(msg);

                        if(msg.status == 'OK')
                        {
                            $('#feedback .result').append('<div class="alert alert-success">Your message has been sent. Thank you!<button type="button" class="close" data-dismiss="alert">×</button></div');
                            form.trigger('reset');
                        }
                        else if(msg.text)
                        {
                            $.each(msg.text, function(i, elem){
                                $('#feedback .result').append('<div class="alert alert-error">' + elem + '<button type="button" class="close" data-dismiss="alert">×</button></div');
                            })
                        }
                        else
                        {
                            $('#feedback .result').append('<div class="alert alert-error">Error<button type="button" class="close" data-dismiss="alert">×</button></div');
                        }

                    }
                })

            });;
        });
        
        return false;
    })
})