const shareData = {
	title: 'MDN',
	text: 'Aprenda desenvolvimento web no MDN!',
	url: 'https://developer.mozilla.org',
}


// Deve ser acionado algum tipo de "ativação do usuário"


body = document.querySelector('body');


function includeShare() {
	if (body.offsetWidth < 769) {
		$(".share-icon").attr("href", `javascript:void(0)`);
		document.querySelector('.share-icon').addEventListener('click', async () => {
			try {
				await navigator.share(shareData);
			} catch(err){
				console.log(err)
			}
		})
	}
	if (body.offsetWidth >= 769) {
		$(".share-icon").attr("href", `#openModal`)
	}
}

window.addEventListener('resize', includeShare);
window.onload = includeShare;